const express = require('express');
const router = express.Router();

const _XlsxController = require('../../controller/master_controller/_XlsxController')

router.get('/xlsx/actual-template', _XlsxController.getActualXlsxTemplate)
router.get('/xlsx/plan-template', _XlsxController.getPlanXlsxTemplate)
router.get('/xlsx/material', _XlsxController.getMaterialDataXlsx)
router.get('/xlsx/material-template', _XlsxController.getMaterialXlsxTemplate)
router.get('/xlsx/avgprice-template/:year', _XlsxController.getAvgPriceUpdateXlsxTemplate)
router.get('/xlsx/supplies-export/:type/:year/:line', _XlsxController.getSuppliesDataXlsxByYearAndLine)
router.get('/xlsx/supplies-update/:year/:line', _XlsxController.getSuppliesUpdateTemplate)
router.get('/xlsx/opex', _XlsxController.getOpexDataXlsx)
router.get('/xlsx/opex-fix', _XlsxController.getOpexFixDataXlsx)
router.get('/xlsx/opex-tr-template', _XlsxController.getOpexTrXlsxTemplate)
router.get('/xlsx/opex-tr/:price/:year/:costCtrId', _XlsxController.getOpexTransactionXlsx)
router.get('/xlsx/costctr', _XlsxController.getCostCenterDataXlsx)

module.exports = router;