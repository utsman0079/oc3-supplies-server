const api = require('../../tools/common')
const tools = require('../../tools/shared')
const XLSX = require('xlsx')
const path = require('path')
const fs = require('fs')
const suppliesActualModel = require('../../model/tr_actual.model');
const suppliesPlanModel = require('../../model/tr_supplies.model');
const costCenterModel = require('../../model/cost_ctr.model')
const materialModel = require('../../model/material.model')
const avgPriceModel = require('../../model/tr_avgprice.model')
const prodplanModel = require('../../model/tr_prodplan.model')
const lineModel = require('../../model/line.model')
const opexModel = require('../../model/opex.model')
const opexFixModel = require('../../model/opex_fix.model')
const opexTrModel = require('../../model/tr_opex.model')

const uploadActualBudget = async (req, res) => {
    try {
        const workbook = XLSX.read(req.file.buffer)
        const sheet = workbook.Sheets[workbook.SheetNames[0]]
        const data = XLSX.utils.sheet_to_json(sheet, { raw: false })

        const injectionData = []
        const notProductionLine = []
        const invalidMaterials = []

        const formatDate = (dateString) => {
            if (/^\d{4}-\d{2}-\d{2}$/.test(dateString)) {
              return dateString;
            }
            let date = new Date(dateString);
            let year = date.getFullYear();
            let month = ('0' + (date.getMonth() + 1)).slice(-2);
            let day = ('0' + date.getDate()).slice(-2);
            
            return `${year}-${month}-${day}`;
        }

        const convertTo24HourFormat = (time12h) => {
            if (/^\d{2}:\d{2}:\d{2}$/.test(time12h)) {
                return time12h;
            }
            
            const [time, period] = time12h.split(' ');
            const [hours, minutes, seconds] = time.split(':');
            
            let hours24 = parseInt(hours, 10);
            
            if (period === 'PM' && hours24 < 12) {
                hours24 += 12;
            } else if (period === 'AM' && hours24 === 12) {
                hours24 = 0;
            }
            
            return `${hours24.toString().padStart(2, '0')}:${minutes}:${seconds}`;
        }

        const filterData = async () => {
            return new Promise((resolve, reject) => {
                data.forEach(async (item, index) => {
                    const costCenter = await costCenterModel.search(+item.cost_ctr)
                    const material = await materialModel.search(+item.material_code)
                    
                    const materialCode = `${item.material_code}`
                    if (materialCode.length !== 9) {
                        invalidMaterials.push(JSON.stringify({ material_code: item.material_code, material_desc: item.material_desc }))
                    }
                    
                    if (material.length === 1 && costCenter.length === 1 && costCenter[0].line_id != null) {
                        item.cost_ctr_id = costCenter[0].id
                        item.material_id = material[0].id
                        item.cost_center = +item.cost_ctr
                        item.material_code = +item.material_code
                        item.quantity = Math.abs(item.quantity.replace(/[^0-9]/g, ''))
                        item.price = Math.abs(item.price.replace(/[^0-9]/g, ''))
                        item.posting_date = formatDate(item.posting_date)
                        item.document_date = formatDate(item.document_date)
                        item.entry_date = formatDate(item.entry_date)
                        item.entry_datetime = `${item.entry_date} ${convertTo24HourFormat(item.time_of_entry)}`

                        const keysToDelete = ['uom', 'cost_ctr', 'material_desc', 'entry_date', 'time_of_entry']
                        keysToDelete.forEach(key => delete item[key])

                        injectionData.push(item)
                    }

                    if (costCenter.length === 1 && costCenter[0].line_id == null) {
                        notProductionLine.push({ materialCode: item.material_code, cost_ctr: item.cost_ctr, section: costCenter[0].section })
                    }

                    if (index === data.length - 1) {
                        setTimeout(() => resolve(), 50)
                    }
                })
            })
        }

        await filterData().then(async () => {
            const finalData = injectionData.filter(item => {
                for (let key in item) {
                    if (key.startsWith('__EMPTY')) {
                      return false;
                    }
                }
                return true;
            })

            await suppliesActualModel.insert(finalData).then(() => {
                return res.status(200).json({
                    status: true,
                    data: {
                        message: "Data inserted successfully",
                        inserted_rows: injectionData.length,
                        not_included_section: tools.getUniqueData(notProductionLine, 'cost_ctr'),
                        invalid_materials: [...new Set(invalidMaterials)].map(obj => JSON.parse(obj))
                    }
                })
            })
        })

    } catch (err) {
        api.catchError(res, err)
    }
}

const uploadPlanBudget = async (req, res) => {
    try {
        const workbook = XLSX.read(req.file.buffer)
        const sheet = workbook.Sheets[workbook.SheetNames[0]]
        const data = XLSX.utils.sheet_to_json(sheet, { raw: false, defval: null })

        const injectionData = []
        const notProductionLine = []

        const filterData = async () => {
            return new Promise((resolve, reject) => {
                data.forEach(async (item, index) => {
                    const costCenter = await costCenterModel.search(+item.cost_ctr)
                    const material = await materialModel.search(+item.material_code)
                    const prodplan = await prodplanModel.getByYearAndLine(item.year, costCenter[0].line_id)
                    const calculation =
                        item.calculation_by === "Prodplan" ? 1
                            : item.calculation_by === "Daily" ? 2
                                : item.calculation_by === "Weekly" ? 3
                                    : item.calculation_by === "Monthly" ? 4
                                        : 0

                    if (costCenter[0].line_id !== null) {
                        prodplan.forEach((prod, index) => {
                            let quantity = 0
                            let price = 0
                            if (calculation == 1) {
                                quantity = prod.prodplan / 1000 * +item.bom
                            } else if (calculation == 2) {
                                quantity = prod.daily_count * +item.bom
                            } else if (calculation == 3) {
                                quantity = prod.weekly_count * +item.bom
                            } else if (calculation == 4) {
                                quantity = 1 * +item.bom
                            }
                            price = quantity * material[0].average_price

                            injectionData.push({
                                budget_id: `${item.year}-${costCenter[0].line_id}-${costCenter[0].id}-${material[0].id}`,
                                material_id: material[0].id,
                                cost_ctr_id: costCenter[0].id,
                                calc_budget_id: calculation,
                                prodplan_id: prod.id,
                                bom: +item.bom,
                                quantity: quantity,
                                price: price
                            })
                        })
                    } else {
                        notProductionLine.push({ cost_ctr: item.cost_ctr, section: costCenter[0].section })
                    }

                    if (index == data.length - 1) {
                        setTimeout(() => {
                            resolve()
                        }, 50)
                    }

                })
            })
        }

        await filterData().then(async () => {
            await suppliesPlanModel.insert(injectionData).then(() => {
                return res.status(200).json({
                    status: true,
                    data: {
                        message: "Data inserted successfully",
                        inserted_rows: injectionData.length,
                        not_included_section: tools.getUniqueData(notProductionLine, 'budget_id')
                    }
                })
            })
        })

    } catch (err) {
        api.catchError(res, err)
    }
}

const uploadMaterial = async (req, res) => {
    try {
        const workbook = XLSX.read(req.file.buffer)
        const sheet = workbook.Sheets[workbook.SheetNames[0]]
        const data = XLSX.utils.sheet_to_json(sheet, { raw: false, defval: null })

        const year = parseInt(req.query.year)

        const materials = [...data].map(item => {
            let data = {...item}
            delete data['average_price']
            return data
        })

        await materialModel.insert(materials).then(async () => {
            const filterAvgPriceData = async () => {
                return new Promise((resolve, reject) => {
                    data.forEach(async (item, index, arr) => {
                        let material = await materialModel.getByCode(item.material_code)
                        if (material.length == 1) {
                            item.material_id = material[0].id
                            item.year = year
                            delete item['material_desc'], delete item['uom'], delete item['material_code']
                        }
                        if (index === data.length - 1) {
                            setTimeout(() => resolve(arr), 50)
                        }
                    })
                })
            }
            
            let avgPriceData = await filterAvgPriceData()
            await avgPriceModel.insert(avgPriceData).then(() => {
                return api.ok(res, { message: "Material data inserted successfully." })
            })    
        })

        

    } catch(err) {
        api.catchError(res, err)
    }
}

const uploadAvgPriceUpdate = async (req, res) => {
    try {
        const workbook = XLSX.read(req.file.buffer)
        const sheet = workbook.Sheets[workbook.SheetNames[0]]
        const data = XLSX.utils.sheet_to_json(sheet, { raw: false, defval: null })
    
        const year = parseInt(req.query.year) || 0
        
        const beginUpdate = async () => {
            return new Promise((resolve, reject) => {
                data.forEach(async (item, index) => {
                    let avgPriceInSelectedYear = await avgPriceModel.getByCodeAndYear(item.material_code, year)
                    
                    if (avgPriceInSelectedYear.length == 1) {
                        await avgPriceModel.update(avgPriceInSelectedYear[0].avg_price_id, { average_price: item.average_price })
                    } else {
                        let material = await materialModel.getByCode(item.material_code)
                        await avgPriceModel.insert({
                            material_id: material[0].id,
                            year: year,
                            average_price: item.average_price
                        })
                    }
        
                    if (index === data.length - 1) {
                        setTimeout(() => resolve(), 50)
                    }
                })
            })
        }
    
        await beginUpdate().then(() => api.ok(res, { message: "Average price updated successfully." }))
    } catch (err) {
        return api.catchError(res, err)
    }

    
}

const getActualXlsxTemplate = async (req, res) => {
    const filePath = path.join(__dirname, "../../../uploads/template/template_supplies_actual.xlsx")
    res.sendFile(filePath)
}

const getPlanXlsxTemplate = async (req, res) => {
    const filePath = path.join(__dirname, "../../../uploads/template/template_supplies_plan.xlsx")
    res.sendFile(filePath)
}

const getMaterialXlsxTemplate = async (req, res) => {
    const filePath = path.join(__dirname, "../../../uploads/template/template_material_insert.xlsx")
    res.sendFile(filePath)
}

const getOpexTrXlsxTemplate = async (req, res) => {
    const filePath = path.join(__dirname, "../../../uploads/template/template_opex_tr_create.xlsx")
    res.sendFile(filePath)
}

const getAvgPriceUpdateXlsxTemplate = async (req, res) => {
    try {
        const getAvgData = async () => {
            const material = await materialModel.getAll()
            const mergeData = await tools.mergeAvgPrice(material)
            return new Promise((resolve, reject) => {
                const data = [];
                const year = parseInt(req.params.year) || 0
                mergeData.forEach((item, index) => {
                    let detailPrice = item.detail_price
                    if (Array.isArray(detailPrice) && detailPrice.length > 0) {
                        let avgPrice = detailPrice.find(val => val.year === year)
                        if (!avgPrice) {
                            avgPrice = detailPrice[detailPrice.length - 1]
                        }
                        data.push({ material_code: item.material_code, material_desc: item.material_desc, average_price: avgPrice.average_price || 0})
                    } else {
                        data.push({ material_code: item.material_code, material_desc: item.material_desc, average_price: 0 })
                    }
    
                    if (index === mergeData.length - 1) {
                        setTimeout(() => resolve(data), 50)
                    }
                })
            })
        }
    
        const avgData = await getAvgData()
        const workbook = XLSX.utils.book_new();
        const worksheet = XLSX.utils.json_to_sheet(avgData);
    
        XLSX.utils.book_append_sheet(workbook, worksheet);
    
        const excelBuffer = XLSX.write(workbook, { type: 'buffer', bookType: 'xlsx' });
    
        res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.set('Content-Disposition', 'attachment; filename="avg_price_template.xlsx"');
        res.send(excelBuffer);
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getSuppliesDataXlsxByYearAndLine = async (req, res) => {
    try {
        if (!isNaN(req.params.year) && !isNaN(req.params.line) && isNaN(req.params.type)) {

            let type = req.params.type
            let transformedData = []

            if (type === "budget") {
                transformedData = await suppliesPlanModel.getByYearAndLine(req.params.year, req.params.line)
                    .then(data => tools.transformSuppliesViewData(data))
            } else if (type === "actual") {
                transformedData = await suppliesActualModel.getActualSuppliesByYearAndLine(req.params.year, req.params.line)
                    .then(data => tools.transformSuppliesViewData(data))
            } else if (type === "comparison") {
                let budget = await suppliesPlanModel.getByYearAndLine(req.params.year, req.params.line)
                    .then(data => tools.transformSuppliesViewData(data))
                let actual = await suppliesActualModel.getActualSuppliesByYearAndLine(req.params.year, req.params.line)
                    .then(data => tools.transformSuppliesViewData(data))
                transformedData = tools.mergeBudgetData(budget, actual)
            }
            
            await suppliesPlanModel.getByYearAndLine(req.params.year, req.params.line);
            let lineName = await lineModel.getById(req.params.line)

            if (lineName.length > 0) {
                lineName = lineName[0].name
            } else {
                return api.error(res, "Couldn't find line with id " + req.params.line, 400)
            }

            if (transformedData.length > 0) {
                const prepareData = async () => {
                    return new Promise((resolve, reject) => {
                        
                        const finalData = []
                        transformedData.forEach(async (item, index) => {
                            const avgPrice = await avgPriceModel.getByCodeAndYear(item.material_code, item.year)

                            let month = {}
                            
                            for (let m = 1; m <= 12; m++) {

                                if (type === 'comparison') {
                                    month[`${tools.getSimpleMonthName(m)} Budget Qty`] = 0
                                    month[`${tools.getSimpleMonthName(m)} Actual Qty`] = 0
                                    month[`${tools.getSimpleMonthName(m)} Budget Price`] = 0
                                    month[`${tools.getSimpleMonthName(m)} Actual Price`] = 0
                                } else {
                                    month[`${tools.getSimpleMonthName(m)} Qty`] = 0
                                    month[`${tools.getSimpleMonthName(m)} Price`] = 0
                                    if (type === 'budget') {
                                        month[`${tools.getSimpleMonthName(m)} Week`] = 0
                                        month[`${tools.getSimpleMonthName(m)} Prodplan`] = 0
                                    }
                                }
                            }


                            item.budgeting_data.forEach((data, i) => {

                                if (type === 'comparison') {
                                    month[`${tools.getSimpleMonthName(data.month)} Budget Qty`] = +data.plan_qty
                                    month[`${tools.getSimpleMonthName(data.month)} Actual Qty`] = +data.actual_qty
                                    month[`${tools.getSimpleMonthName(data.month)} Budget Price`] = +data.plan_price
                                    month[`${tools.getSimpleMonthName(data.month)} Actual Price`] = +data.actual_price
                                } else {
                                    month[`${tools.getSimpleMonthName(data.month)} Qty`] = +data.quantity
                                    month[`${tools.getSimpleMonthName(data.month)} Price`] = +data.price
                                    if (type === 'budget') {
                                        month[`${tools.getSimpleMonthName(data.month)} Week`] = data.total_week
                                        month[`${tools.getSimpleMonthName(data.month)} Prodplan`] = +data.prodplan
                                    }
                                }

                                if (i === item.budgeting_data.length - 1) {

                                    if (type === 'comparison') {
                                        const totals = { "Total Budget Qty": 0, "Total Budget Price": 0, "Total Actual Qty": 0, "Total Actual Price": 0 }

                                        for (const key in month) {
                                            if (month.hasOwnProperty(key)) {
                                                if (key.includes("Budget Qty")) { totals["Total Budget Qty"] += month[key]; } 
                                                else if (key.includes("Budget Price")) { totals["Total Budget Price"] += month[key]; } 
                                                else if (key.includes("Actual Qty")) { totals["Total Actual Qty"] += month[key]; } 
                                                else if (key.includes("Actual Price")) { totals["Total Actual Price"] += month[key]; }
                                            }
                                        }

                                        month["Total Budget Qty"] = totals["Total Budget Qty"]
                                        month["Total Actual Qty"] = totals["Total Actual Qty"]
                                        month["Total Budget Price"] = totals["Total Budget Price"]
                                        month["Total Actual Price"] = totals["Total Actual Price"]

                                    } else {
                                        const totals = { "Total Qty": 0, "Total Price": 0, "Total Week": 0, "Total Prodplan": 0 };
                                    
                                        for (const key in month) {
                                            if (month.hasOwnProperty(key)) {
                                                if (key.includes("Qty")) { totals["Total Qty"] += month[key]; } 
                                                else if (key.includes("Price")) { totals["Total Price"] += month[key]; } 
                                                else if (key.includes("Week")) { totals["Total Week"] += month[key]; } 
                                                else if (key.includes("Prodplan")) { totals["Total Prodplan"] += month[key]; }
                                            }
                                        }
    
                                        month["Total Qty"] = totals["Total Qty"]
                                        month["Total Price"] = totals["Total Price"]
                                        if (type === "budget") {
                                            month["Total Week"] = totals["Total Week"]
                                            month["Total Prodplan"] = totals["Total Prodplan"]
                                        }
                                    }
                                    
                                }

                                
                            })

                            finalData.push({
                                "Section": item.section,
                                "Cost Center": item.cost_center,
                                "Concatenate": `${item.cost_center}-${item.material_code}`,
                                "Material Code": item.material_code,
                                "Material Description": item.material_desc,
                                "Calculation Method": item.calculation_by ? (item.calculation_by == 'Prodplan' ? `Prodplan (Per 1000 Btl)` : item.calculation_by) : undefined,
                                "UOM": item.uom,
                                "Average Price": avgPrice.length == 1 ? (+avgPrice[0].average_price || 0) : 0,
                                "BOM": type === "budget" ? +item.bom : undefined,
                                ...month
                            })

                            if (index === transformedData.length - 1) {
                                setTimeout(() => resolve(finalData), 50)
                            }
                        })
                    })
                }
                prepareData().then((finalData) => {
                    const workbook = XLSX.utils.book_new();
                    const worksheet = XLSX.utils.json_to_sheet(finalData);
                
                    XLSX.utils.book_append_sheet(workbook, worksheet);
                
                    const excelBuffer = XLSX.write(workbook, { type: 'buffer', bookType: 'xlsx' });
                
                    res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    res.set('Content-Disposition', `attachment; filename="SUPPLIES ${type.toUpperCase()} (${lineName} ${req.params.year}).xlsx"`);
                    res.send(excelBuffer);
                })
            } else {
                return api.error(res, `Supplies ${type} data in line ${lineName} ${req.params.year} is empty`, 400);
            }
        } else {
            return api.error(res, "Year and line id is invalid", 400);
        }
    } catch (err) {
        return api.catchError(res, err)
    }
    
}

const getSuppliesUpdateTemplate = async (req, res) => {
    try {
        const year = parseInt(req.params.year) || 0
        const line = parseInt(req.params.line) || 0
        let data = await suppliesPlanModel.getByYearAndLine(year, line).then((finalData) => tools.transformSuppliesViewData(finalData))

        if (data.length > 0) {
            const lineName = await lineModel.getById(line).then(line => line[0].name)
            const getSuppliesData = async () => {
                return new Promise((resolve, reject) => {
                    const finalData = []
                    data.forEach(async (item, index) => {
                        const newRow = {
                            cost_center: +item.cost_center,
                            material_code: item.material_code,
                            material_desc: item.material_desc,
                            bom: +item.bom,
                        }
                        finalData.push(newRow)
                        if (index === data.length - 1) {
                            setTimeout(() => resolve(finalData), 50)
                        }
                    })
                })
            }
    
            const suppliesData = await getSuppliesData()

            const workbook = XLSX.utils.book_new();
            const worksheet = XLSX.utils.json_to_sheet(suppliesData);
        
            XLSX.utils.book_append_sheet(workbook, worksheet);

            const buffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    
            res.setHeader('Content-Disposition', `attachment; filename=update_template_${lineName}_${year}.xlsx`);
            res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            res.send(buffer);
        } else {
            return api.error(res, "Data in the selected line and year is empty", 400)
        }

    } catch (err) {
        return api.catchError(res, err)
    }

}

const uploadSuppliesBudgetUpdate = async (req, res) => {
    let errCode = 500
    try {
        const year = parseInt(req.query.year) || 0
        const lineId = parseInt(req.query.lineId) || 0

        const workbook = XLSX.read(req.file.buffer)
        const sheet = workbook.Sheets[workbook.SheetNames[0]]
        const data = XLSX.utils.sheet_to_json(sheet, { raw: false, defval: null })

        const updateData = async () => {
            return new Promise((resolve, reject) => {
                data.forEach(async (item, index) => {
                    const costCenterId = await costCenterModel.getByCostCenter(item.cost_center).then(cost => cost[0].id)
                    const materialId = await materialModel.getByCode(item.material_code).then(mat => mat[0].id)
                    const avgPrice = await avgPriceModel.getByCodeAndYear(item.material_code, year).then(avg => +avg[0].average_price)
                    let budgetId = `${year}-${lineId}-${costCenterId}-${materialId}`
                    let bom = +item.bom
                    const supplies = await suppliesPlanModel.getByBudgetId(budgetId)

                    if (supplies.length === 12) {
                        supplies.forEach(async supply => {
                            let quantity = 0
                            let price = 0

                            if (supply.calculation_by === "Prodplan") {
                                quantity = supply.prodplan / 1000 * bom
                            } else if (supply.calculation_by === "Daily") {
                                quantity = 21 * bom
                            } else if (supply.calculation_by === "Weekly") {
                                quantity = supply.total_week * bom
                            } else if (supply.calculation_by === "Monthly") {
                                quantity = 1 * bom
                            }
                            price = quantity * avgPrice

                            let finalData ={
                                bom: bom,
                                quantity: quantity,
                                price: price
                            }

                            await suppliesPlanModel.update(supply.supplies_id, finalData)
                        })
                    } else {
                        errCode = 400
                        reject(Error(`Couldn't find supply ${item.cost_center}-${item.material_code} in selected line`))
                    }

                    if (index === data.length - 1) {
                        setTimeout(() => resolve(true), 50)
                    }
                })
            })
        }

        await updateData().then(() => {
            return api.ok(res, { message: "Data updated successfully" })
        })

    } catch (err) {
        return api.catchError(res, err, errCode)
    }
}

const getMaterialDataXlsx = async (req, res) => {
    try {
        const material = await materialModel.getAll()
        const mergeData = await tools.mergeAvgPrice(material)

        const prepareData = async () => {
            return new Promise((resolve, reject) => {
                const finalData = []
                mergeData.forEach((item, index) => {
                    let avgPriceYear = {}
                    item.detail_price.forEach(price => {
                        avgPriceYear[`Avg Price (${price.year})`] = price.average_price || 0
                    })
                    finalData.push({
                        "Material Code": item.material_code,
                        "Material Description": item.material_desc,
                        "UOM": item.uom,
                       ...avgPriceYear
                    })
                    if (index === mergeData.length - 1) {
                        setTimeout(() => resolve(finalData), 50)
                    }
                })
            })
        }
        
        const finalData = await prepareData().then(data => data.sort((a, b) => a["Material Code"] - b["Material Code"]))
        
        const workbook = XLSX.utils.book_new();
        const worksheet = XLSX.utils.json_to_sheet(finalData);
    
        XLSX.utils.book_append_sheet(workbook, worksheet);
    
        const excelBuffer = XLSX.write(workbook, { type: 'buffer', bookType: 'xlsx' });
    
        res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.set('Content-Disposition', `attachment; filename="material_export-${new Date().getTime()}.xlsx"`);
        res.send(excelBuffer);
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getOpexDataXlsx = async (req, res) => {
    try {
        const opex = await opexModel.getAll();
        const prepareData = async () => {
            return new Promise((resolve, reject) => {
                const finalData = []
                opex.forEach((item, index) => {
                    finalData.push({
                        'Order': item.order,
                        'Description': item.description,
                        'Type': item.type,
                        "COAR": item.coar
                    })
                    if (index === opex.length - 1) {
                        setTimeout(() => resolve(finalData), 50)
                    }
                })
            })
        }
        
        const finalData = await prepareData().then(data => data.sort((a, b) => a["Order"] - b["Order"]))
        
        const workbook = XLSX.utils.book_new();
        const worksheet = XLSX.utils.json_to_sheet(finalData);
        
        XLSX.utils.book_append_sheet(workbook, worksheet);
        
        const excelBuffer = XLSX.write(workbook, { type: 'buffer', bookType: 'xlsx' });
        
        res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.set('Content-Disposition', `attachment; filename="opex_export-${new Date().getTime()}.xlsx"`);
        res.send(excelBuffer);
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getOpexFixDataXlsx = async (req, res) => {
    try {
        const opex = await opexFixModel.getAll();
        const prepareData = async () => {
            return new Promise((resolve, reject) => {
                const finalData = []
                opex.forEach((item, index) => {
                    finalData.push({ 'Chart Of Account': `${item.order} - ${item.name}` })
                    if (index === opex.length - 1) {
                        setTimeout(() => resolve(finalData), 50)
                    }
                })
            })
        }
        
        const finalData = await prepareData().then(data => data.sort((a, b) => a["Order"] - b["Order"]))
        
        const workbook = XLSX.utils.book_new();
        const worksheet = XLSX.utils.json_to_sheet(finalData);
        
        XLSX.utils.book_append_sheet(workbook, worksheet);
        
        const excelBuffer = XLSX.write(workbook, { type: 'buffer', bookType: 'xlsx' });
        
        res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.set('Content-Disposition', `attachment; filename="opex_export-${new Date().getTime()}.xlsx"`);
        res.send(excelBuffer);
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getOpexTransactionXlsx = async (req, res) => {
    try {
        const price = req.params.price == 'actual' ? 'actual' : req.params.price == 'budget' ? 'budget' : 'comparison'
        const costCtrId = parseInt(req.params.costCtrId) || 0
        const section = await costCenterModel.getById(costCtrId)
        const year = parseInt(req.params.year) || 0
        const opex = await opexTrModel.getByCostCtrAndYear(costCtrId, year).then(data => tools.transformOpexTrViewData(data));
        const prepareData = async () => {
            return new Promise((resolve, reject) => {
                const finalData = []
                opex.forEach((item, index) => {
                    let months = {}

                    for (let m = 1; m <= 12; m++) {
                        if (price === 'comparison') {
                            months[tools.getSimpleMonthName(m) + ` Budget`] = 0
                            months[tools.getSimpleMonthName(m) + ` Actual`] = 0
                        } else {
                            months[tools.getSimpleMonthName(m)] = 0
                        }
                    }

                    item.budgeting_data.forEach(data => {
                        if (price === 'comparison') {
                            months[tools.getSimpleMonthName(data.month) + ` Budget`] = data.budget
                            months[tools.getSimpleMonthName(data.month) + ` Actual`] = data.actual
                        } else {
                            months[tools.getSimpleMonthName(data.month)] = data[price]
                        }
                    })

                    finalData.push({
                        'Chart Of Account': `${item.order} - ${item.opex}`,
                        ...months
                    })
                    if (index === opex.length - 1) {
                        setTimeout(() => resolve(finalData), 50)
                    }
                })
            })
        }
        
        const finalData = await prepareData().then(data => data.sort((a, b) => a["Order"] - b["Order"]))
        
        const workbook = XLSX.utils.book_new();
        const worksheet = XLSX.utils.json_to_sheet(finalData);
        
        XLSX.utils.book_append_sheet(workbook, worksheet);
        
        const excelBuffer = XLSX.write(workbook, { type: 'buffer', bookType: 'xlsx' });
        
        res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.set('Content-Disposition', `attachment; filename="${price.toUpperCase()}-OPEX-${section.length > 0 ? section[0].section : ''}-${year}.xlsx"`);
        res.send(excelBuffer);
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getCostCenterDataXlsx = async (req, res) => {
    try {
        const costCenter = await costCenterModel.getAll();
        const prepareData = async () => {
            return new Promise((resolve, reject) => {
                const finalData = []
                costCenter.forEach((item, index) => {
                    finalData.push({
                        'Cost Center': item.cost_ctr,
                        'Section': item.section,
                        "Line": item.line,
                        'Language': item.language,
                        'COAR': item.coar,
                        'COCD': item.cocd,
                        'CCTC': item.cctc,
                        'Valid From': item.valid_from,
                        'Valid To': item.valid_to,
                    })
                    if (index === costCenter.length - 1) {
                        setTimeout(() => resolve(finalData), 50)
                    }
                })
            })
        }
        
        const finalData = await prepareData().then(data => data.sort((a, b) => a["Cost Center"] - b["Cost Center"]))
        
        const workbook = XLSX.utils.book_new();
        const worksheet = XLSX.utils.json_to_sheet(finalData);
        
        XLSX.utils.book_append_sheet(workbook, worksheet);
        
        const excelBuffer = XLSX.write(workbook, { type: 'buffer', bookType: 'xlsx' });

        res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.set('Content-Disposition', `attachment; filename="costcenter_export-${new Date().getTime()}.xlsx"`);
        res.send(excelBuffer);
    } catch (err) {
        return api.catchError(res, err)
    }
}

const inject = async (req, res) => {
    try {
        const workbook = XLSX.read(req.file.buffer)
        const sheet = workbook.Sheets[workbook.SheetNames[0]]
        const data = XLSX.utils.sheet_to_json(sheet)
        
        const prepareData = async () => {
            return new Promise((resolve, reject) => {
                data.forEach(async (item, index) => {
                    const parts = item.chart_of_account.split(' - ');
                    item.order = +parts[0];
                    item.name = parts.slice(1).join(' - ');
                    delete item.chart_of_account
                    if (index === data.length - 1) {
                        setTimeout(() => resolve(true), 50)
                    }
                })
            })
        }

        prepareData().then(() => {
            return api.ok(res, data)
        })
        

    } catch (err) {
        return api.catchError(res, err)
    }
    
}

const uploadOpexTransaction = async (req, res) => {
    try {
        const type = req.params.type == 'update' ? 'update' : 'create'
        const price = req.params.price == 'actual' ? 'actual' : 'budget'
        const year = parseInt(req.query.year) || 0
        const costCtrId = parseInt(req.query.costCtrId) || 0
        let lineId = await costCenterModel.getById(costCtrId)

        if (lineId.length === 1 && lineId[0].line_id) {
            lineId = lineId[0].line_id
        } else {
            return api.error(res, `Invalid factory line`, 400)
        }

        const workbook = XLSX.read(req.file.buffer)
        const sheet = workbook.Sheets[workbook.SheetNames[0]]
        const data = XLSX.utils.sheet_to_json(sheet, { defval: 0 })

        const months = Object.keys(data[0]).map((item, index) => ({ number: index, name: item}))
        months.splice(0, 1)

        const prepareData = async () => {
            return new Promise((resolve, reject) => {
                const finalData = []
                
                const prepare = async (dataItem, opexId) => {
                    return new Promise((_resolve, _reject) => {
                        months.forEach(async (month, _index) => {
                            finalData.push({
                                month: month.number,
                                budget_id: `${year}-${lineId}-${costCtrId}-${opexId}`,
                                year: year,
                                cost_ctr_id: costCtrId,
                                opex_id: opexId,
                                [price]: dataItem[month.name],
                            })

                            if (_index === months.length - 1) {
                                setTimeout(() => _resolve(finalData), 50)
                            }
                        })
                    })
                }

                let newFinalData = []
                data.forEach(async(item, index) => {
                    const parts = item['Chart Of Account'].split(' - ');
                    const order = +parts[0];
                    const existingOrder = await opexFixModel.getByOrder(order)

                    if (existingOrder.length === 1) {
                        const opexId = existingOrder[0].id
                        newFinalData = await prepare(item, opexId)
                    }

                    if (index === data.length - 1) {
                        setTimeout(() => resolve(newFinalData), 150)
                    }
                })
            })
        }

        await prepareData().then(async(finalData) => {
            if (type === 'update') {
                const updatedOpex = finalData.map((opex) => ({data: {...opex}}))
                const update = async () => new Promise((resolve, reject) => {
                    updatedOpex.forEach(async (opex, index) => {
                        const opexTr = await opexTrModel.getByBudgetIdAndMonth(opex.data.budget_id, opex.data.month)
                        opex.id = opexTr.length > 0 ? opexTr[0].tr_opex_id : null

                        await opexTrModel.update(opex.id, opex.data)

                        if (index === updatedOpex.length - 1) {
                            setTimeout(() => resolve(true), 50)
                        }
                    })
                })
                
                await update().then(() => {
                    return api.ok(res, {message: "Data updated successfully"})
                })
                
            } else {
                await opexTrModel.insert(finalData).then(() => {
                    return api.ok(res, {message: "Data inserted successfully"})
                })
                
            }
            
        })
    } catch (err) {
        return api.catchError(res, err)
    }
}

module.exports = {
    getPlanXlsxTemplate,
    getActualXlsxTemplate,
    uploadPlanBudget,
    uploadActualBudget,
    uploadMaterial,
    uploadAvgPriceUpdate,
    getMaterialXlsxTemplate,
    getAvgPriceUpdateXlsxTemplate,
    getSuppliesDataXlsxByYearAndLine,
    getSuppliesUpdateTemplate,
    getOpexTrXlsxTemplate,
    uploadSuppliesBudgetUpdate,
    getMaterialDataXlsx,
    getOpexDataXlsx,
    getOpexFixDataXlsx,
    getOpexTransactionXlsx,
    getCostCenterDataXlsx,
    inject,
    uploadOpexTransaction
}