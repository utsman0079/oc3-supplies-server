const model = require('../../model/tr_supplies.model');
const api = require('../../tools/common')
const tools = require('../../tools/shared')

const getAllSuppliesBudget = async (req, res) => {
    try {
        let data = await model.getAll();
        return api.ok(res, data);
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getSuppliesByYearAndLine = async (req, res) => {
    if (!isNaN(req.params.year) && !isNaN(req.params.line)) {
        let data = await model.getByYearAndLine(req.params.year, req.params.line);
        if (data.length > 0) {
            let transformedData = tools.transformSuppliesViewData(data)
            transformedData.forEach(item => {
                // item.average_price = +item.average_price
                item.bom = +item.bom
                item.budgeting_data.forEach(data => {
                    data.prodplan = +data.prodplan
                    data.quantity = +data.quantity
                    data.price = +data.price
                })
            })
            return api.ok(res, transformedData);
        } else {
            return api.ok(res, data);
        }
    } else {
        return api.error(res, "Bad Request", 400);
    }
}

const getSuppliesByYearAndCostCenter = async (req, res) => {
    if (!isNaN(req.params.year) && !isNaN(req.params.costctr)) {
        let data = await model.getByYearAndCostCenter(req.params.year, req.params.costctr)
        if (data.length > 0) {
            let transformedData = tools.transformSuppliesViewData(data)
            return api.ok(res, transformedData);
        } else {
            return api.ok(res, data);
        }
    } else {
        return api.error(res, "Bad Request", 400);
    }
}

const getSuppliesBudgetById = async (req, res) => {
    if (!isNaN(req.params.id)) {
        let data = await model.getById(req.params.id);
        return api.ok(res, data);
    } else {
        return api.error(res, "Bad Request", 400);
    }
}

const insertSuppliesBudget = async (req, res) => {
    try {
        let data = await model.insert(req.body.form_data);
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const updateSuppliesBudget = async (req, res) => {
    try {
        let data = await model.updateByBudgetId(req.params.budgetid, req.body.form_data);
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const isBudgetIdExist = async (req, res) => {
    let budgetId = req.params.budgetid
    try {
        let data = await model.getByBudgetId(budgetId)
        if (data.length == 0) {
            return res.json({
                is_available: true,
                message: 'Budget ID available'
            })
        }
        return res.json({
            is_available: false,
            length: data.length,
            message: 'Budget ID already exists!'
        })
        
    } catch (err) {
        return api.catchError(res, err)
    }
}

const updateWithBudgetAndProdplanId = async (req, res) => {
    try {
        let budgetId = req.params.budgetid
        let requestData = req.body.form_data
        if (Array.isArray(requestData) && requestData.length == 12) {
            requestData.forEach(data => {
                model.updateByBudgetAndProdplanId(budgetId, data.prodplan_id, data)
            })
            return api.ok(res, requestData)
        } else return api.error(res, `Data length is less than 12`, 400)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const updateMultipleSupplies = async (req, res) => {
    try {
        let requestData = req.body.form_data
        const update = async (data) => {
            return new Promise((resolve, reject) => {
                data.forEach(async (item, index) => {
                    await model.updateByBudgetId(item.budget_id, item.data)
                    if (index === data.length - 1) {
                        resolve(true)
                    }
                })
            })
        }
        update(requestData).then(() => {
            return api.ok(res, requestData)
        })
    } catch (err) {
        return api.catchError(res, err)
    }
}

module.exports = {
    getAllSuppliesBudget,
    getSuppliesBudgetById,
    insertSuppliesBudget,
    updateSuppliesBudget,
    getSuppliesByYearAndLine,
    getSuppliesByYearAndCostCenter,
    isBudgetIdExist,
    updateMultipleSupplies,
    updateWithBudgetAndProdplanId
}