const model = require('../../model/opex_dashboard.model');
const api = require('../../tools/common')

const getOpexPerLineByYear = async (req, res) => {
    try {
        const year = parseInt(req.query.year) || 0;
        let data = await model.getPerLineByYear(year)
            .then(opex => opex.map(a => ({ ...a, budget: a.budget ? +a.budget : 0, actual: a.actual ? +a.actual : 0 })))
        return api.ok(res, data);
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getOpexPerSectionMonthByYearAndLine = async (req, res) => {
    try {
        const year = parseInt(req.query.year) || 0;
        const lineId = parseInt(req.query.lineId) || 0;
        let data = await model.getPerSectionMonthByYearAndLine(year, lineId)
            .then(opex => opex.map(a => ({ ...a, budget: a.budget ? +a.budget : 0, actual: a.actual ? +a.actual : 0 })))
        return api.ok(res, data);
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getPerOpexByYearAndCostCtrId = async (req, res) => {
    try {
        const year = parseInt(req.query.year) || 0;
        const costCtrId = parseInt(req.query.costCtrId) || 0;
        let data = await model.getPerOpexByYearAndCostCtrId(year, costCtrId)
            .then(opex => opex.map(a => ({ ...a, budget: a.budget ? +a.budget : 0, actual: a.actual ? +a.actual : 0 })))
        return api.ok(res, data);
    } catch (err) {
        return api.catchError(res, err)
    }
}

module.exports = {
    getOpexPerLineByYear,
    getOpexPerSectionMonthByYearAndLine,
    getPerOpexByYearAndCostCtrId,
}