const model = require('../../model/opex.model');
const api = require('../../tools/common')

const searchByPagination = async (req, res) => {
    try {
        const page = parseInt(req.query.page) || 1;
        const pageSize = parseInt(req.query.pageSize) || 25;
        const offset = (page - 1) * pageSize;
        const sortColumn = req.query.sortColumn || ''
        const sortDirection = req.query.sortDirection || ''
        const term = req.query.search || ''
        const data = await model.searchPagination(term, offset, pageSize, sortColumn, sortDirection)
        const total = await model.getSearchLength(term)

        return res.json({ status: true, total_opex: total, data: data })
    } catch (err) {
        return api.catchError(res, err);
    }
}

const insertOpex = async (req, res) => {
    try {
        let data = await model.insert(req.body.form_data);
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const updateOpex = async (req, res) => {
    try {
        let data = await model.update(req.params.id, req.body.form_data);
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

module.exports = {
    searchByPagination,
    insertOpex,
    updateOpex,
}