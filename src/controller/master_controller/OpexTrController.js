const model = require('../../model/tr_opex.model');
const api = require('../../tools/common')
const tools = require('../../tools/shared')

const insertOpexTr = async (req, res) => {
    try {
        const data = await model.insert(req.body.form_data)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const checkOpexAvailibility = async (req, res) => {
    try {
        const budgetId = req.params.budgetId
        const data = await model.getByBudgetId(budgetId)
        if (data.length > 0) {
            return api.error(res, "Opex is already exists on selected section", 400)
        } else {
            return api.ok(res, { message: "Available" })
        }
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getOpexTrByCostCenterAndYear = async (req, res) => {
    try {
        const costCtrId = parseInt(req.params.costCtrId) || 0;
        const year = parseInt(req.params.year) || 0;
        const data = await model.getByCostCtrAndYear(costCtrId, year).then((opex) => tools.transformOpexTrViewData(opex))
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const searchPaginationByCostCtrYear = async (req, res) => {
    try {
        const costCtrId = parseInt(req.params.costCtrId) || 0;
        const year = parseInt(req.params.year) || 0;

        const page = parseInt(req.query.page) || 1;
        const pageSize = parseInt(req.query.pageSize) || 25;
        const offset = (page - 1) * pageSize;
        const sortColumn = req.query.sortColumn || ''
        const sortDirection = req.query.sortDirection || ''
        const term = req.query.search || ''

        const data = await model.searchPaginationByCostCtrYear(
            costCtrId, year, term, offset, pageSize, sortColumn, sortDirection
        ).then((items) => tools.transformOpexTrViewData(items))
        const total = await model.getSearchLengthByCostCtrYear(costCtrId, year, term)

        return res.json({ status: true, total: total, data: data })
    } catch (err) {
        return api.catchError(res, err)
    }
}

const updateMultipleOpex = async (req, res) => {
    try {
        let requestData = req.body.form_data
        const update = async (data) => {
            return new Promise((resolve, reject) => {
                data.forEach(async (item, index) => {
                    await model.update(item.id, item.data)
                    if (index === data.length - 1) {
                        resolve(true)
                    }
                })
            })
        }
        update(requestData).then(() => {
            return api.ok(res, requestData)
        })
    } catch (err) {
        return api.catchError(res, err)
    }
}

const updateMultipleOpexByBudgetId = async (req, res) => {
    try {
        let requestData = req.body.form_data
        const update = async (data) => {
            return new Promise((resolve, reject) => {
                data.forEach(async (item, index) => {
                    await model.updateByBudgetId(item.budget_id, item.data)
                    if (index === data.length - 1) {
                        resolve(true)
                    }
                })
            })
        }
        update(requestData).then(() => {
            return api.ok(res, requestData)
        })
    } catch (err) {
        return api.catchError(res, err)
    }
}

module.exports = {
    insertOpexTr,
    checkOpexAvailibility,
    getOpexTrByCostCenterAndYear,
    searchPaginationByCostCtrYear,
    updateMultipleOpexByBudgetId,
    updateMultipleOpex
}