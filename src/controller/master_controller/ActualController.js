const model = require('../../model/tr_actual.model');
const suppliesModel = require('../../model/tr_supplies.model')
const api = require('../../tools/common')
const tools = require('../../tools/shared')

const getMergedActualBudgetSupplies = async (req, res) => {
    try {
        const budgetId = `${req.params.budgetId}`
        const budgetPlanData = await suppliesModel.getByBudgetId(budgetId)
            .then(data => tools.transformSuppliesViewData(data))
        const budgetActualData = await model.getByBudgetId(budgetId)
            .then(data => tools.transformSuppliesViewData(data))

        const mergedData = tools.mergeBudgetData(budgetPlanData, budgetActualData)
        return api.ok(res, mergedData)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getActualTransaction = async (req, res) => {
    try {
        const page = parseInt(req.query.page) || 1;
        const pageSize = parseInt(req.query.pageSize) || 25;
        const offset = (page - 1) * pageSize;
        const sortColumn = req.query.sortColumn || ''
        const sortDirection = req.query.sortDirection || ''
        const term = req.query.search || '';

        const lineId = parseInt(req.query.lineId) || -1
        const costCtrId = parseInt(req.query.costCtrId) || -1

        const fromDate = req.query.from
        const toDate = req.query.to

        const finalData = await model.getActualTransaction(term, offset, pageSize, sortColumn, sortDirection, fromDate, toDate, lineId, costCtrId)

        return res.json({ status: true, total: finalData.total, value: finalData.value, data: finalData.data })
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getActualTransactionDetail = async (req, res) => {
    try {
        const materialCode = parseInt(req.params.materialCode) || 0
        const lineId = parseInt(req.query.lineId) || -1
        const costCtrId = parseInt(req.query.costCtrId) || -1
        const [lastMonth, lastYear] = await model.getLastEntryDateData(materialCode, lineId, costCtrId).then((data) => {
            if (data.length <= 0) {
                return ["1", "1"]
            }
            return [
                new Date(data[0].entry_datetime).getMonth() + 1, 
                new Date(data[0].entry_datetime).getFullYear()
            ]
        })
        const fromDate = req.query.from || getDefaultDate('from', lastYear, lastMonth)
        const toDate = req.query.to || getDefaultDate('to', lastYear, lastMonth)

        let data = await model.getTransactionDetail(materialCode, fromDate, toDate, lineId, costCtrId)
        return api.ok(res, data)
        
    } catch (err) {
        return api.catchError(res, err)
    }
}

const insertActualBudget = async (req, res) => {
    try {
        let data = await model.insert(req.body.form_data)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getActualSuppliesByYearAndLine = async (req, res) => {
    if (!isNaN(req.params.year) && !isNaN(req.params.line)) {
        let data = await model.getActualSuppliesByYearAndLine(req.params.year, req.params.line);
        if (data.length > 0) {
            let transformedData = tools.transformSuppliesViewData(data)
            transformedData.forEach(item => {
                item.average_price = +item.average_price
                item.budgeting_data.forEach(data => {
                    data.quantity = +data.quantity
                    data.price = +data.price
                })
            })
            return api.ok(res, transformedData);
        } else {
            return api.ok(res, data);
        }
    } else {
        return api.error(res, "Bad Request", 400);
    }
}

const getActualPerLineByYear = async (req, res) => {
    try {
        const year = parseInt(req.query.year) || 0;
        let data = await model.getActualPerLineByYear(year);
        data.forEach(item => item.price = +item.price)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getActualPerSectionByLine = async (req, res) => {
    try {
        const year = parseInt(req.query.year) || 0;
        const lineId = parseInt(req.query.lineId) || 0;
        let data = await model.getActualPerSectionByLine(year, lineId);
        data.forEach(item => item.price = +item.price)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getActualPerSectionMonthByLine = async (req, res) => {
    try {
        const year = parseInt(req.query.year) || 0;
        const lineId = parseInt(req.query.lineId) || 0;
        let data = await model.getActualPerSectionMonthByLine(year, lineId);
        data.forEach(item => item.price = +item.price)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getActualPerSupplyByLine = async (req, res) => {
    try {
        const year = parseInt(req.query.year) || 0;
        const lineId = parseInt(req.query.lineId) || 0;
        let data = await model.getActualPerSupplyByLine(year, lineId);
        data.forEach(item => item.price = +item.price)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    } 
}

const getActualProdplanByYearAndLine = async (req, res) => {
    if (!isNaN(req.params.year) && !isNaN(req.params.line)) {
        let data = await model.getProdplanByYearAndLine(req.params.year, req.params.line);
        data.forEach(item => item.prodplan = +item.prodplan)
        return api.ok(res, data);
    } else {
        return api.error(res, "Bad Request", 400);
    }
}

function getDefaultDate(type, year, month) {
    const getLastDayOfMonth = (_year, _month) => {
        _month -= 1
        return new Date(_year, _month + 1, 0).getDate();
    }
    const lastDayOfMonth = getLastDayOfMonth(year, month)
    const monthFilter = month < 10 ? `0${month}` : `${month}`

    if (type === 'from') {
        return `${year}-${monthFilter}-01`
    } else {
        return `${year}-${monthFilter}-${lastDayOfMonth}`
    }
}

module.exports = {
    getMergedActualBudgetSupplies,
    getActualTransactionDetail,
    getActualTransaction,
    insertActualBudget,
    getActualSuppliesByYearAndLine,
    getActualPerLineByYear,
    getActualPerSectionByLine,
    getActualPerSectionMonthByLine,
    getActualPerSupplyByLine,
    getActualProdplanByYearAndLine
}