const db = require('../database/supplies.config')

const getAll = async () => await db("mst_opex_fix").select("*").where("is_removed", 0)

const searchPagination = async (term, offset, pageSize, sortColumn, sortDirection) => {
    return await db("mst_opex_fix")
       .select("*")
       .where("is_removed", 0)
       .andWhere(builder => {
            builder.where(db.raw('CAST(`order` AS CHAR)'), "like", `%${term}%`)
               .orWhere("name", "like", `%${term}%`)
        })
       .orderBy(sortColumn ? sortColumn : 'order', sortDirection ? sortDirection : 'asc')
       .offset(offset)
       .limit(pageSize)
}

const getSearchLength = async (term) => {
    const length = await db("mst_opex_fix")
       .count("id", { as: "total_opex" })
       .where("is_removed", 0)
       .andWhere(builder => {
            builder.where(db.raw('CAST(`order` AS CHAR)'), "like", `%${term}%`)
               .orWhere("name", "like", `%${term}%`)
        })

    return +length[0].total_opex
}

const getById = async (id) => await db("mst_opex_fix").select("*").where("id", id)

const insert = async (data) => await db("mst_opex_fix").insert(data)

const update = async (id, data) => await db("mst_opex_fix").where("id", id).update(data)

const getByOrder = async (order) => await db("mst_opex_fix").select("*").where("order", order).andWhere("is_removed", 0)

const search = async (term) =>
    await db("mst_opex_fix")
       .select("*")
       .where("is_removed", 0)
       .where(builder => {
        builder.where(db.raw('CAST(`order` AS CHAR)'), "like", `%${term}%`)
            .orWhere("name", "like", `%${term}%`)
        })

module.exports = {
    getAll,
    getById,
    insert,
    getByOrder,
    search,
    searchPagination,
    getSearchLength,
    update
}