const db = require("../database/supplies.config");

const getPerLineByYear = async (year) =>
    await db("v_opex_budget as vob")
        .select("vob.line_id", "vob.line")
        .sum("vob.budget as budget")
        .sum("vob.actual as actual")
        .where("vob.year", year)
        .groupBy("vob.line_id");

const getPerSectionMonthByYearAndLine = async (year, lineId) =>
    await db("v_opex_budget as vob")
        .select(db.raw("CONCAT(vob.cost_ctr_id, '-', vob.month) as `key`"),
            "vob.cost_ctr_id", "vob.section", "vob.month"
        )
        .sum("vob.budget as budget")
        .sum("vob.actual as actual")
        .where("vob.year", year)
        .andWhere("vob.line_id", lineId)
        .groupBy("vob.cost_ctr_id", "vob.month");

const getPerOpexByYearAndCostCtrId = async (year, costCtrId) =>
    await db('v_opex_budget as vob')
        .select('vob.budget_id', 'vob.order', 'vob.opex')
        .sum('vob.budget as budget')
        .sum('vob.actual as actual')
        .where('vob.year', year)
        .andWhere('vob.cost_ctr_id', costCtrId)
        .groupBy('vob.budget_id');

module.exports = {
    getPerLineByYear,
    getPerSectionMonthByYearAndLine,
    getPerOpexByYearAndCostCtrId,
}
