const db = require('../database/supplies.config')

const getByBudgetIdAndMonth = async (budgetId, month) => 
    await db("v_opex_budget")
        .select("*")
        .where("budget_id", budgetId)
        .where("month", month)

const getByLineAndYear = async (lineId, year) =>
    await db("v_opex_budget")
       .select("*")
       .where("line_id", lineId)
       .andWhere("year", year)

const getByCostCtrAndYear = async (costCtrId, year) => 
    await db("v_opex_budget")
        .select("*")
        .where("cost_ctr_id", costCtrId)
        .andWhere("year", year)

const searchPaginationByCostCtrYear = async (costCtrId, year, term, offset, pageSize, sortColumn, sortDirection) => {
    return await db("v_opex_budget")
        .select("*")
        .where("cost_ctr_id", costCtrId)
        .where("year", year)
        .where(builder => {
            builder.where(db.raw('CAST(`order` AS CHAR)'), "like", `%${term}%`)
                .orWhere("opex", "like", `%${term}%`)
                .orWhere("line", "like", `%${term}%`)
                .orWhere("section", "like", `%${term}%`)
                .orWhere("cost_ctr", "like", `%${term}%`)
                .orWhere(db.raw(`CAST(budget AS CHAR)`), "like", `%${term}%`)
                .orWhere(db.raw(`CAST(actual AS CHAR)`), "like", `%${term}%`)
        })
        .orderBy(sortColumn ? sortColumn : 'order', sortDirection ? sortDirection : 'asc')
        .offset(offset)
        .limit(pageSize)
}

const getSearchLengthByCostCtrYear = async (costCtrId, year, term) => {
    const length = await db("v_opex_budget")
        .count("tr_opex_id", { as: "total_opex_budget" })
        .where("cost_ctr_id", costCtrId)
        .where("year", year)
        .where(builder => {
            builder.where(db.raw('CAST(`order` AS CHAR)'), "like", `%${term}%`)
                .orWhere("opex", "like", `%${term}%`)
                .orWhere("line", "like", `%${term}%`)
                .orWhere("section", "like", `%${term}%`)
                .orWhere("cost_ctr", "like", `%${term}%`)
                .orWhere(db.raw(`CAST(budget AS CHAR)`), "like", `%${term}%`)
                .orWhere(db.raw(`CAST(actual AS CHAR)`), "like", `%${term}%`)
        })
    return +length[0].total_opex_budget
}

const getByBudgetId = async (budgetId) =>
    await db("v_opex_budget")
       .select("*")
       .where("budget_id", budgetId)

const insert = async (data) => await db("tr_opex").insert(data)

const update = async (id, data) => await db("tr_opex").where("id", id).update(data)

const updateByBudgetId = async (budgetId, data) => await db("tr_opex").where("budget_id", budgetId).update(data)

module.exports = {
    getByBudgetIdAndMonth,
    getByCostCtrAndYear,
    getByLineAndYear,
    searchPaginationByCostCtrYear,
    getSearchLengthByCostCtrYear,
    update,
    insert,
    getByBudgetId,
    updateByBudgetId
}