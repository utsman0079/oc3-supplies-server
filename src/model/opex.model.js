const db = require("../database/supplies.config");

const getAll = async () => await db.select("*").from("mst_opex").where("is_removed", 0)

const getById = async (id) =>
  await db("mst_opex").where("id", id).where("is_removed", 0);

const getByOrder = async (order) =>
  await db("mst_opex").where("order", order).where("is_removed", 0);

const searchPagination = async (term, offset, pageSize, sortColumn, sortDirection) =>
  await db("mst_opex")
    .where("is_removed", 0)
    .andWhere((builder) => {
      builder
        .where("description", "like", `%${term}%`)
        .orWhere(db.raw("CAST(`order` AS CHAR)"), "like", `%${term}%`)
        .orWhere(db.raw("CAST(coar AS CHAR)"), "like", `%${term}%`)
        .orWhere("type", "like", `%${term}%`);
    })
    .orderBy(sortColumn ? sortColumn : 'order', sortDirection ? sortDirection : 'asc')
    .offset(offset)
    .limit(pageSize);

const getSearchLength = async (term) => {
  const length = await db("mst_opex")
    .count("id", { as: "total_opex" })
    .where("is_removed", 0)
    .andWhere((builder) => {
      builder
        .where("description", "like", `%${term}%`)
        .orWhere(db.raw("CAST(`order` AS CHAR)"), "like", `%${term}%`)
        .orWhere(db.raw("CAST(coar AS CHAR)"), "like", `%${term}%`)
        .orWhere("type", "like", `%${term}%`);
    });
  return +length[0].total_opex;
};

const insert = async (data) => await db("mst_opex").insert(data);

const update = async (id, data) =>
  await db("mst_opex").where("id", id).update(data);

module.exports = {
  getAll,
  getById,
  getByOrder,
  searchPagination,
  getSearchLength,
  insert,
  update,
};
