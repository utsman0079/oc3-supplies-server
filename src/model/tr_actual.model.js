const db = require("../database/supplies.config");

const getAll = async () => await db.select("*").from("tr_actual_budget").where("is_removed", 0)

const getLastEntryDateData = async (materialCode, lineId = -1, costCtrId = -1) => {
    let query = db('tr_actual_budget as tab')
        .select('tab.*')
        .leftJoin(
            'mst_factory_line_cost_center as mflcc',
            'tab.cost_center',
            'mflcc.cost_ctr'
        )
        .where('tab.is_removed', 0)
        .where("tab.material_code", materialCode)

    if (lineId && lineId !== -1) {
        query = query.andWhere("mflcc.line_id", lineId)
    }

    if (costCtrId && costCtrId !== -1) {
        query = query.andWhere("mflcc.id", costCtrId)
    }

    return await query.orderBy('entry_datetime', 'desc').limit(1)
}

const getTransactionDetail = async (materialCode, fromDate, toDate, lineId = -1, costCtrId = -1) => {
    let query = db('tr_actual_budget as tab')
        .select(
            'mflcc.section',
            'mfl.name as line',
            'mflcc.line_id',
            'tab.*'
        )
        .leftJoin(
            'mst_factory_line_cost_center as mflcc',
            'tab.cost_center',
            'mflcc.cost_ctr'
        )
        .leftJoin('mst_factory_line as mfl', 'mflcc.line_id', 'mfl.id')
        .where('tab.is_removed', 0)
        .where('tab.material_code', materialCode)
        .andWhereBetween('tab.posting_date', [`${fromDate}`, `${toDate}`])

    if (lineId && lineId !== -1) {
        query = query.andWhere('mfl.id', lineId)
    }

    if (costCtrId && costCtrId !== -1) {
        query = query.andWhere('mflcc.id', costCtrId)
    }

    return await query.orderBy('tab.posting_date');
}

const getActualTransaction = async (term, offset, pageSize, sortCol, sortDir, fromDate, toDate, lineId = -1, costCtrId = -1) => {
    let baseQuery = db('tr_actual_budget as tab')
        .select(
            'mms.material_desc',
            'mms.uom',
            'mflcc.section',
            'mfl.name as line',
            'mflcc.line_id',
            'tab.*'
        )

    let countQuery = db('tr_actual_budget as tab').count("tab.id", { as: 'total' })

    let sumPriceQuery = db('tr_actual_budget as tab').sum("tab.price as price")

    const processQuery = (queryInput) => {
        let query = queryInput
            .leftJoin('mst_material_supplies as mms', 'tab.material_id', 'mms.id')
            .leftJoin(
                'mst_factory_line_cost_center as mflcc',
                'tab.cost_ctr_id',
                'mflcc.id'
            )
            .leftJoin('mst_factory_line as mfl', 'mflcc.line_id', 'mfl.id')
            .where('tab.is_removed', 0)
            .where(builder => {
                builder.where("mfl.name", "like", `%${term}%`)
                    .orWhere("mflcc.section", "like", `%${term}%`)
                    .orWhere("mms.material_desc", "like", `%${term}%`)
                    .orWhere(db.raw(`CAST(tab.material_code AS CHAR)`), "like", `%${term}%`)
                    .orWhere(db.raw(`CAST(tab.cost_center AS CHAR)`), "like", `%${term}%`)
                    .orWhere("tab.reservation", "like", `%${term}%`)
                    .orWhere("tab.reference", "like", `%${term}%`)
                    .orWhere("tab.user_name", "like", `%${term}%`)
            })

        if (fromDate && toDate) {
            query = query.andWhereBetween('tab.posting_date', [`${fromDate}`, `${toDate}`])
        }

        if (lineId && lineId !== -1) {
            query = query.andWhere('mfl.id', lineId)
        }

        if (costCtrId && costCtrId !== -1) {
            query = query.andWhere('mflcc.id', costCtrId)
        }

        return query
    }

    const total = await processQuery(countQuery).then(data => +data[0].total)
    const value = await processQuery(sumPriceQuery).then(data => +data[0].price)
    const data = await processQuery(baseQuery)
        .orderBy(sortCol ? sortCol : 'entry_datetime', sortDir ? sortDir : 'desc')
        .offset(offset)
        .limit(pageSize)
    return { total, value, data }
}

const getByBudgetId = async (budgetId) =>
    await db("v_actual_budget")
        .select("*")
        .whereRaw(`CONCAT(year, '-', line_id, '-', cost_ctr_id, '-', material_id) = '${budgetId}'`)

const insert = async (data) => await db("tr_actual_budget").insert(data)

const update = async (id, data) => await db("tr_actual_budget").where("id", id).update(data)

const getActualSuppliesByYearAndLine = async (year, lineId) =>
    await db.select("*").from("v_actual_budget").where("year", year).where("line_id", lineId)

// Include Dashboard API

const getActualPerLineByYear = async (year) =>
    await db('v_actual_budget as vab')
        .select('vab.line_id', 'vab.line')
        .sum('vab.price as price')
        .where('vab.year', year)
        .groupBy('vab.line_id');

const getActualPerSectionByLine = async (year, lineId) =>
    await db('v_actual_budget as vab')
        .select('vab.cost_ctr_id', 'vab.section')
        .sum('vab.price as price')
        .where('vab.year', `${year}`)
        .andWhere('vab.line_id', lineId)
        .groupBy('vab.cost_ctr_id');

const getActualPerSectionMonthByLine = async (year, lineId) =>
    await db('v_actual_budget as vab')
        .select('vab.month', 'vab.section', 'vab.cost_ctr_id')
        .sum('vab.price as price')
        .where('vab.year', `${year}`)
        .andWhere('vab.line_id', lineId)
        .groupBy('vab.section', 'vab.month');

const getActualPerSupplyByLine = async (year, lineId) =>
    await db('v_actual_budget as vab')
        .select('vab.budget_id', 'vab.material_code', 'vab.material_desc', 'vab.section')
        .sum('vab.price as price')
        .where('vab.year', `${year}`)
        .andWhere('vab.line_id', lineId)
        .groupBy('vab.budget_id');

const getProdplanByYearAndLine = async (year, lineId) =>
    await db
        .select("*")
        .from("tr_actual_prodplan")
        .where("year", year)
        .where("line_id", lineId)
        .where("is_removed", 0)
        .orderByRaw("length(`month`), `month`");

module.exports = {
    getAll,
    getByBudgetId,
    getLastEntryDateData,
    getTransactionDetail,
    getActualTransaction,
    insert,
    update,
    getActualSuppliesByYearAndLine,
    getActualPerLineByYear,
    getActualPerSectionByLine,
    getActualPerSectionMonthByLine,
    getActualPerSupplyByLine,
    getProdplanByYearAndLine,
}