const materialModel = require('../model/material.model')

function getUniqueData(arr, property) {
    let uniqueData = {};
    let result = [];

    for (let obj of arr) {
        let value = obj[property];
        if (!uniqueData[value]) {
            uniqueData[value] = obj;
            result.push(obj);
        }
    }

    return result;
}

async function mergeAvgPrice(dataSource) {
    return new Promise((resolve, reject) => {
        if (dataSource.length > 0) {
            dataSource.forEach(async (item, index) => {
                let avgPrice = await materialModel.getAvgPriceByCode(item.material_code)
                item.detail_price = []
                if (avgPrice.length > 0) {
                    avgPrice.forEach((price) => {
                        item.detail_price.push({
                            year: price.year || null,
                            avg_price_id: price.avg_price_id || null,
                            average_price: +price.average_price || null,
                            updated_at: price.updated_at ? price.updated_at : price.created_at
                        })
                    })
                }
                delete item['average_price']
                if (index === dataSource.length - 1) {
                    setTimeout(() => resolve(dataSource), 50)
                }
            })
        } else resolve([])
    })
}

function transformSuppliesViewData(data) {
    const transformedArray = []
    if (Array.isArray(data)) {
        data.forEach(originalObj => {
            const existingObject = transformedArray.find((transformedObj) => transformedObj.budget_id === originalObj.budget_id);
            if (existingObject) {
                existingObject.budgeting_data.push({
                    supplies_id: originalObj.supplies_id,
                    month: originalObj.month,
                    prodplan: originalObj.prodplan,
                    total_week: originalObj.total_week,
                    quantity: originalObj.quantity,
                    price: originalObj.price,
                    prodplan_id: originalObj.prodplan_id
                });
            } else {
                const newObj = {
                    budget_id: originalObj.budget_id,
                    line: originalObj.line,
                    year: originalObj.year,
                    section: originalObj.section,
                    cost_center: originalObj.cost_center,
                    material_code: originalObj.material_code,
                    material_desc: originalObj.material_desc,
                    calculation_by: originalObj.calculation_by,
                    uom: originalObj.uom,
                    // average_price: originalObj.average_price,
                    bom: originalObj.bom,
                    calculation_id: originalObj.calculation_id,
                    cost_ctr_id: originalObj.cost_ctr_id,
                    line_id: originalObj.line_id,
                    material_id: originalObj.material_id,
                    budgeting_data: [
                        {
                            supplies_id: originalObj.supplies_id,
                            month: originalObj.month,
                            prodplan: originalObj.prodplan,
                            total_week: originalObj.total_week,
                            quantity: originalObj.quantity,
                            price: originalObj.price,
                            prodplan_id: originalObj.prodplan_id
                        },
                    ],
                };
    
                transformedArray.push(newObj);
            }
        })
    }
    
    return transformedArray
} 

function mergeBudgetData(plan, actual) {
    const mergedData = [];

    plan.forEach(planItem => {
        const matchingActual = actual.find((actualItem) => actualItem.budget_id === planItem.budget_id);
        const mergedItem = {
            budget_id: planItem.budget_id,
            line: planItem.line,
            year: planItem.year,
            cost_ctr_id: planItem.cost_ctr_id,
            cost_center: planItem.cost_center,
            section: planItem.section,
            material_code: planItem.material_code,
            material_desc: planItem.material_desc,
            uom: planItem.uom,
            budgeting_data: []
        };

        if (matchingActual) {
            for (let i = 1; i <= 12; i++) {
                const planBudget = planItem.budgeting_data.find((data) => data.month === i) || { quantity: 0, price: 0 };
                const actualBudget = matchingActual.budgeting_data.find((data) => data.month === i) || { quantity: 0, price: 0 };

                mergedItem.budgeting_data.push({
                    month: i,
                    plan_qty: planBudget.quantity,
                    plan_price: planBudget.price,
                    actual_qty: actualBudget.quantity,
                    actual_price: actualBudget.price
                });
            }
        } else {
            planItem.budgeting_data.forEach((data) => {
                mergedItem.budgeting_data.push({
                    month: data.month,
                    plan_qty: data.quantity,
                    plan_price: data.price,
                    actual_qty: 0,
                    actual_price: 0
                });
            });
        }

        mergedData.push(mergedItem);
    });

    actual.forEach(actualItem => {
        const matchingPlan = plan.find((planItem) => planItem.budget_id === actualItem.budget_id);
        if (!matchingPlan) {
            const mergedItem = {
                budget_id: actualItem.budget_id,
                line: actualItem.line,
                year: actualItem.year,
                cost_center: actualItem.cost_center,
                cost_ctr_id: actualItem.cost_ctr_id,
                section: actualItem.section,
                material_code: actualItem.material_code,
                material_desc: actualItem.material_desc,
                uom: actualItem.uom,
                budgeting_data: []
            };

            actualItem.budgeting_data.forEach((data) => {
                mergedItem.budgeting_data.push({
                    month: data.month,
                    plan_qty: 0,
                    plan_price: 0,
                    actual_qty: data.quantity,
                    actual_price: data.price
                });
            });

            mergedData.push(mergedItem);
        }
    });

    return mergedData;
}

function transformOpexTrViewData(data) {
    const transformedArray = []
    if (Array.isArray(data)) {
        data.forEach(originalObj => {
            const existingObject = transformedArray.find((transformedObj) => transformedObj.budget_id === originalObj.budget_id);
            if (existingObject) {
                existingObject.budgeting_data.push({
                    tr_opex_id: originalObj.tr_opex_id,
                    month: originalObj.month,
                    budget: +originalObj.budget,
                    actual: +originalObj.actual,
                });
            } else {
                const newObj = {
                    budget_id: originalObj.budget_id,
                    year: originalObj.year,
                    line: originalObj.line,
                    section: originalObj.section,
                    cost_ctr: originalObj.cost_ctr,
                    order: originalObj.order,
                    opex: originalObj.opex,
                    line_id: originalObj.line_id,
                    cost_ctr_id: originalObj.cost_ctr_id,
                    opex_id: originalObj.opex_id,
                    budgeting_data: [
                        {
                            tr_opex_id: originalObj.tr_opex_id,
                            month: originalObj.month,
                            budget: +originalObj.budget,
                            actual: +originalObj.actual,
                        },
                    ],
                };
    
                transformedArray.push(newObj);
            }
        })
    }
    
    return transformedArray
} 

function getSimpleMonthName(monthNumber) {
    const monthNames = [
      "Jan", "Feb", "Mar", "Apr", "May", "Jun",
      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    
    return monthNames[monthNumber - 1];
  }

module.exports = {
    getUniqueData,
    mergeAvgPrice,
    transformSuppliesViewData,
    mergeBudgetData,
    transformOpexTrViewData,
    getSimpleMonthName
}
